import random
import re

import vk_api
from settings import *
from vk_api.longpoll import VkLongPoll, VkEventType


class VkBotGroup:

    def __init__(self):
        self.session = vk_api.VkApi(token=TOKEN)
        self.long_poll = VkLongPoll(self.session)
        self.vk = self.session.get_api()

        self.allowed_users = open('allowed_users.txt').read().split()

        self.commands = {'/comm': 'self.send_comment_from_group(event)', '/pm': 'self.send_message_from_group(event)',
                         '/command_list': 'self.command_list(event)', '/add': 'self.add_user_to_allowed_users(event)',
                         '/allowed_users': 'self.show_all_allowed_users(event)'}

    def listen_chat(self):
        for event in self.long_poll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text and event.from_user:
                try:
                    eval(self.commands[event.text.split()[0]])
                except:
                    self.send_msg(user_id=event.user_id,
                                  msg="Нет такой команды, напишите '/command_list' для того чтобы узнать все "
                                      "команды ")

    def send_msg(self, msg, user_id):
        self.vk.messages.send(
            user_id=user_id,
            message=msg,
            random_id=random.randint(1, 10 ** 10)
        )

    def send_msg_domain(self, msg, domain):
        self.vk.messages.send(
            domain=domain,
            message=msg,
            random_id=random.randint(1, 10 ** 10)
        )

    def create_comment(self, owner_id, post_id, msg):
        self.vk.wall.createComment(
            owner_id=owner_id,
            post_id=post_id,
            message=msg
        )

    def get_user_id_from_domain(self, user_domain):
        user = self.vk.users.get(users_ids=user_domain,name_case='nom')
        print(user)

    def send_comment_from_group(self, event):
        try:
            command, link, msg = event.text.split()
            link = re.findall('wall.*', link)[0][4:]
            owner_id, post_id = link.split('_')
            try:
                self.create_comment(owner_id, post_id, msg)
                self.send_msg(user_id=event.user_id, msg='Комментарий {} успешно размещен на {}'.format(msg, link))
            except vk_api.ApiError as err:
                self.send_msg(user_id=event.user_id, msg='Комментарий не был размещен ({})'.format(err))
        except:
            self.send_msg(user_id=event.user_id, msg='Неправильно набрана команда, правильный пример:\n /comm '
                                                     'Ссылка Сообщение')

    def send_msg_domain_or_id(self, link, msg, event):
        id_or_domain = link.split('/')[-1]
        status_msg = 'Сообщение успешно отправлено на {}'.format(link)
        try:
            if id_or_domain.startswith('id'):
                self.send_msg(user_id=id_or_domain[2:], msg=msg)
                self.send_msg(user_id=event.user_id, msg=status_msg)
            else:
                self.send_msg_domain(domain=id_or_domain, msg=msg)
                self.send_msg(user_id=event.user_id, msg=status_msg)
        except vk_api.ApiError as err:
            status_msg = 'Сообщение не было отправлено ( {} )'.format(err)
            self.send_msg(user_id=event.user_id, msg=status_msg)

    def send_message_from_group(self, event):
        try:
            command, link, msg = event.text.split()
            if event.user_id in self.allowed_users or event.user_id in ADMIN:
                self.send_msg_domain_or_id(link, msg, event)
            else:
                self.send_msg('У вас недостаточно прав чтобы совершить это', event.user_id)
        except:
            self.send_msg('Неправильно набрана команда, правильный пример:\n /pm Ссылка_на_пользователя Сообщение',
                          user_id=event.user_id)

    def show_all_allowed_users(self, event):
        self.send_msg('Список id привилигированных пользователей: {}'.format(self.allowed_users), event.user_id)

    def add_user_to_allowed_users(self, event):
        try:
            command, user_id = event.text.split()
            if user_id.isdigit() and event.user_id in ADMIN:
                self.allowed_users.append(user_id)
                open('allowed_users.txt', 'a').write(user_id+'\n')
                self.send_msg('Пользователь {} успешно добавлен в список!'.format(user_id), event.user_id)
            elif not user_id.isdigit() and event.user_id in ADMIN:
                self.send_msg('Пожалуйста введите id пользователя в формате 999999999', event.user_id)
            else:
                self.send_msg('У вас недостаточно прав чтобы это сделать', event.user_id)

        except:
            self.send_msg('Неправильно набрана команда, правильный пример:\n /add id_пользователя в формате 999999999',
                          event.user_id)

    def command_list(self, event):
        self.send_msg(user_id=event.user_id, msg=COMMAND_LIST)


if __name__ == '__main__':
    bot = VkBotGroup()
    bot.listen_chat()
