# Constants

# token of your group
TOKEN = ''

# commands that shows when you type "/command_list"
COMMAND_LIST = '/comm - Оставить комментарий под постом от имени группы\n' \
               '/pm - Отправить личное сообщение от имени группы\n' \
               '/add - Добавить пользователя в список привилигированных пользователей\n' \
               '/allowed_users - Посмотреть список всех привилигированных пользователей\n' \


# admin that can change list of allowed users
ADMIN = [486499973, 301681431]  # MUST BE INT
